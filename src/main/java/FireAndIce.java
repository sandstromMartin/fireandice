import com.sun.security.jgss.GSSUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.naming.spi.ObjectFactoryBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class FireAndIce {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the characters number: ");
        String charNum = scan.nextLine();

        //connection and get values of the url by characters and added the input number. Identifing the character with the id number.
        //try with new connection to the url with all characters. catch if the there is a problem with reading the url.
        try {
            URL url = new URL("https://anapioficeandfire.com/api/characters/" + charNum);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            //create the connection if it succed
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();
                //create json objects of the content from the read input (url) as a string.

                JSONObject json = new JSONObject(content.toString());
                //convert the json objects to printable object wih each key value.
                Object name = json.getString("name");
                Object culture = json.getString("culture");
                Object gender = json.getString("gender");
                Object born = json.getString("born");

                System.out.println("---------------");
                System.out.println("Name: " + name);
                System.out.println("Culture: " + culture);
                System.out.println("Gender: " + gender);
                System.out.println("Born: " + born);
                System.out.println("---------------");
                System.out.println("");

                //all names of members

                System.out.println("Would you like to display the names of all sworn members of the previous characters house? (Y/N)");
                String firstChoise = scan.nextLine();
                if (firstChoise.equalsIgnoreCase("Y")) {
                    //store the url from each allegiances into a jsonarray
                    JSONArray allegiances = (JSONArray) json.get("allegiances");
                    if (!allegiances.isEmpty() ) {

                        //for each object in allegiance, a new connection with the url that is stored in allegiance is created and get the content and make it readable.
                        for (Object allegiance : allegiances) {
                            URL url2 = new URL((String) allegiance);
                            HttpURLConnection con2 = (HttpURLConnection) url2.openConnection();
                            con2.setRequestMethod("GET");
                            if (con2.getResponseCode() == HttpURLConnection.HTTP_OK) {
                                InputStreamReader isr2 = new InputStreamReader(con2.getInputStream());
                                BufferedReader br2 = new BufferedReader(isr2);
                                StringBuffer content2 = new StringBuffer();


                                while ((inputLine = br2.readLine()) != null) {
                                    content2.append(inputLine);
                                }
                                br2.close();


                                //create json objects of the content from the read input (url) as a string.  and store the values of swornmembers into a jsonarray
                                JSONObject json2 = new JSONObject(content2.toString());
                                JSONArray swornMembers = (JSONArray) json2.get("swornMembers");
                                //for each object in swornmebers, a new connection with the url that is stored in member is created and get the content and make it readable.

                                for (Object member : swornMembers) {
                                    URL url3 = new URL((String) member);
                                    HttpURLConnection con3 = (HttpURLConnection) url3.openConnection();
                                    con3.setRequestMethod("GET");
                                    if (con3.getResponseCode() == HttpURLConnection.HTTP_OK) {
                                        InputStreamReader isr3 = new InputStreamReader(con3.getInputStream());
                                        BufferedReader br3 = new BufferedReader(isr3);
                                        StringBuffer content3 = new StringBuffer();

                                        while ((inputLine = br3.readLine()) != null) {
                                            content3.append(inputLine);
                                        }
                                        br3.close();

                                        //saves the name om swornmembers in objects that makes the json value printable

                                        JSONObject json3 = new JSONObject(content3.toString());
                                        Object nameOfSworn = json3.getString("name");

                                        System.out.println("---Sworn Members----");
                                        System.out.println(nameOfSworn);
                                        System.out.println("--------------------");
                                    } else System.out.println("No sworn members");

                                }

                            }
                        }

                    }
                    else System.out.println("No sworn members for the allegiance! \nBut here are the pov characters of the books: \n");
                } else if (firstChoise.equalsIgnoreCase("N")) {
                    System.out.println("Ok, but here are the pov characters of the books: \n");
                } else System.out.println("Wrong input value, but here are the pov characters of the books: \n!");

                //pov character
                {

                    //setup
                    URL url4 = new URL("https://anapioficeandfire.com/api/books");
                    HttpURLConnection con4 = (HttpURLConnection) url4.openConnection();
                    con4.setRequestMethod("GET");

                    if (con4.getResponseCode() == HttpURLConnection.HTTP_OK) {

                        InputStreamReader isr4 = new InputStreamReader(con4.getInputStream());
                        BufferedReader br4 = new BufferedReader(isr4);
                        String inputLine4;
                        StringBuffer content4 = new StringBuffer();

                        while ((inputLine4 = br4.readLine()) != null) {
                            content4.append(inputLine4);
                        }
                        br4.close();

                        //similar to above to identify bantam books and the povcharacters for his books.


                        ArrayList<JSONObject> booksByBantam = new ArrayList<>();
                        JSONArray json4 = new JSONArray(content4.toString());
                        for (Object o : json4) {
                            JSONObject json5 = new JSONObject(o.toString());
                            String publisher = json5.getString("publisher");
                            if (publisher.equalsIgnoreCase("Bantam Books")) {
                                booksByBantam.add(json5);
                            }
                        }

                        for (JSONObject jobj : booksByBantam) {
                           System.out.println("Book: "+jobj.getString("name"));

                            JSONArray json5 = jobj.getJSONArray("povCharacters");
                            //create new connection the url of povcharacter that includes in the jsonarray
                            for (Object obj : json5) {
                                URL url5 = new URL((String) obj);
                                HttpURLConnection con5 = (HttpURLConnection) url5.openConnection();
                                con5.setRequestMethod("GET");

                                if (con5.getResponseCode() == HttpURLConnection.HTTP_OK) {
                                    InputStreamReader isr5 = new InputStreamReader(con5.getInputStream());
                                    BufferedReader br5 = new BufferedReader(isr5);
                                    String inputline;
                                    StringBuffer content5 = new StringBuffer();
                                    while ((inputline = br5.readLine()) != null) {
                                        content5.append(inputline);
                                    }
                                    br5.close();
                                    //convert jsonobject to printable objects

                                    JSONObject json6 = new JSONObject();
                                    json6 = new JSONObject(content5.toString());
                                    Object name2 = json6.get("name");


                                    System.out.println("Pov Character: "+name2);


                                }

                            } System.out.println();
                        }

                    }

                }

            }

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}