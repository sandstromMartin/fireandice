# fireAndIce

A console based backend project that connects to an [API of Fire and Ice](https://anapioficeandfire.com/)
Used JDK 14 and Gradle to construct the application. 

When we start the application it allow the user to lookup a character by id number. (Display basic info fo that specific character. ) Example "625"

Then optionally allow the user to display the names of all sworn members of the characters house. (Input "y" or "n")
This can be a lot of characters. If there is no more members of the characters house, a message will be displayed. 
 (Example of id: "1" for no )

No matter what input, the application will finally lookup and sort all pov characters for each books published by Bantam Books. 